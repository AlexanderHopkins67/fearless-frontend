import { useState, useEffect } from "react"
import { useDefaultState } from "./hooks"

export default function AttendConference() {
    const [conferences, setConferences] = useState([])
    const conferenceSelect = useDefaultState()
    const name = useDefaultState()
    const email = useDefaultState()
    const [success, setSuccess] = useState(false)

    const fetchData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/')
        if (response.ok){
            const data = await response.json()
            setConferences(data.conferences)
        }
    }

    useEffect(() => {
        fetchData()
    },[])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {
            "name": name.state,
            "conference": conferenceSelect.state,
            "email": email.state
        }

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch("http://localhost:8001/api/attendees/", fetchConfig)
        if (response.ok) {
            setSuccess(true)
        }

    }

    return (
        <div className="my-5">
        <div className="row">
          <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                {!success ?
                    <form id="create-attendee-form" onSubmit={handleSubmit}>
                        <h1 className="card-title">It's Conference Time!</h1>
                        <p className="mb-3">
                            Please choose which conference
                            you'd like to attend.
                        </p>
                        {!conferences ?
                            <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                <div className="spinner-grow text-secondary" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div>
                        :
                            <div className="mb-3">
                                <select name="conference" id="conference" className="form-select" value={conferenceSelect.state} onChange={conferenceSelect.handleChange} required>
                                    <option key="defaultConf" value="">Choose a conference</option>
                                    {conferences.map(conference => <option key={conference.href} value={conference.href}>{conference.name}</option>)}
                                </select>
                            </div>
                        }
                        <p className="mb-3">
                            Now, tell us about yourself.
                        </p>
                        <div className="row">
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input required placeholder="Your full name" type="text" id="name" name="name" className="form-control" value={name.state} onChange={name.handleChange}/>
                                    <label htmlFor="name">Your full name</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input required placeholder="Your email address" type="email" id="email" name="email" className="form-control" value={email.state} onChange={email.handleChange}/>
                                    <label htmlFor="email">Your email address</label>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-lg btn-primary" type="submit">I'm going!</button>
                        </form>
                        :
                        <div className="alert alert-success mb-0" id="success-message">
                            Congratulations! You're all signed up!
                        </div>
                    }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}