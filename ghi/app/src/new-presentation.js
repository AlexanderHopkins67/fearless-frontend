import { useDefaultState } from "./hooks"
import { useEffect, useState } from "react"

export default function NewPresentation() {
    const [conferences, setConferences] = useState([])
    const name = useDefaultState()
    const email = useDefaultState()
    const company = useDefaultState()
    const title = useDefaultState()
    const synopsis = useDefaultState()
    const conference = useDefaultState()


    const fetchData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/')
        if (response.ok) {
            const data = await response.json()
            setConferences(data.conferences)
            console.log(data.conferences)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {
            "presenter_name": name.state,
            "company_name": company.state,
            "presenter_email": email.state,
            "title": title.state,
            "synopsis": synopsis.state,
        }
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }

        const response = await fetch(`http://localhost:8000/api/conferences/${conference.state}/presentations/`, fetchConfig)
        if (response.ok) {
            name.reset("")
            email.reset("")
            company.reset("")
            title.reset("")
            synopsis.reset("")
            conference.reset("")
        }

    }

    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form id="create-presentation-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input value={name.state} onChange={name.handleChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={email.state} onChange={email.handleChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input value={company.state} onChange={company.handleChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={title.state} onChange={title.handleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea value={synopsis.state} onChange={synopsis.handleChange} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select value={conference.state} onChange={conference.handleChange} required name="conference" id="conference"className="form-select">
                  <option key="defaultPres" value="">Choose a conference</option>
                  {conferences.map(conference => <option key={conference.href} value={conference.id}>{conference.name}</option>)}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
    </div>
    )
}