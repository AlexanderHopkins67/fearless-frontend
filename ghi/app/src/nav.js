import { Link } from 'react-router-dom';

export default function Nav() {
    return (
        <header>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">Conference GO!</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link to='/'>Home</Link>
                </li>
                <li className="nav-item">
                  <Link to='/locations/new'>New Location</Link>
                </li>
                <li className="nav-item">
                  <Link to='/conferences/new'>New Conference</Link>
                </li>
                <li className="nav-item">
                  <Link to='/attendees/new'>New Attendee</Link>
                </li>
                <li className="nav-item">
                  <Link to='/presentation/new'>New Presentation</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    )
}