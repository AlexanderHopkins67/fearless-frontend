import {useEffect, useState} from "react";

export default function LocationForm(){
    const [states, setStates] = useState([])
    const [name, setName] = useState('')
    const [roomCount, setRoomCount] = useState('')
    const [city, setCity] = useState('')
    const [location, setLocation] = useState('')

    const fetchData = async () => {
        const response = await fetch('http://localhost:8000/api/states/');

        if (response.ok) {
            const data = await response.json();
            setStates(data.states)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (event) => {
        event.preventDefault()
        setName(event.target.value)
    }

    const handleRoomChange = (event) => {
        event.preventDefault()
        setRoomCount(event.target.value)
    }

    const handleCityChange = (event) => {
        event.preventDefault()
        setCity(event.target.value)
    }

    const handleLocationChange = (event) => {
        event.preventDefault()
        setLocation(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {
            "name": name,
            "room_count": roomCount,
            "city": city,
            "state": location,
        }

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch('http://localhost:8000/api/locations/', fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()
            console.log(newLocation)
        }

    }
    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form id="create-location-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Name" name="name" onChange={handleNameChange} value={name} required type="text" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Room count" onChange={handleRoomChange} value={roomCount} required type="number" id="room_count" name="room_count" className="form-control"/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="City" onChange={handleCityChange} value={city} required type="text" id="city" name="city" className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select required id="state" name="state" className="form-select" onChange={handleLocationChange} value={location}>
                  <option key="default" value="">Choose a state</option>
                  {states.map((state) => {
                    return <option key={state.abbreviation} value={state.abbreviation}>{state.name}</option>
                  })}
                </select>
              </div>
              <button className="btn btn-primary" type="submit">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}