import React from 'react';
import { Outlet } from 'react-router-dom'
import Nav from "./nav"


function App() {
  return (
    <React.Fragment>
      <div className="container">
        <Nav />
        <Outlet />
      </div>
    </React.Fragment>
        );
    }

export default App;
