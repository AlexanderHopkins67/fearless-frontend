import { useState, useEffect } from "react"
import { useDefaultState } from "./hooks"

export default function NewConference() {
    const [locations, setLocations] = useState([])
    const name = useDefaultState()
    const start = useDefaultState()
    const end = useDefaultState()
    const description = useDefaultState()
    const maxPresentations = useDefaultState()
    const maxAttendees = useDefaultState()
    const place = useDefaultState()

    const fetchData = async () => {
        const response = await fetch('http://localhost:8000/api/locations/')
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {
            "name": name.state,
            "starts": start.state,
            "ends": end.state,
            "description": description.state,
            "max_presentations": maxPresentations.state,
            "max_attendees": maxAttendees.state,
            "location": place.state
        }

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch("http://localhost:8000/api/conferences/", fetchConfig)
        if (response.ok) {
            const newConference = await response.json()
            console.log(newConference)
        }

    }

    return (
        <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a new Conference</h1>
                  <form id="create-conference-form" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input placeholder="Name" name="name" value={name.state} onChange={name.handleChange} required type="text" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="" value={start.state} onChange={start.handleChange} required type="date" id="starts" name="starts" className="form-control"/>
                        <label htmlFor="starts">Starts</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="" value={end.state} onChange={end.handleChange} required type="date" id="ends" name="ends" className="form-control"/>
                        <label htmlFor="ends">Ends</label>
                    </div>
                    <div className="form-floating mb-3">
                        <textarea className="form-control" value={description.state} onChange={description.handleChange} name="description" id="description" cols="30" rows="10"></textarea>
                        <label htmlFor="description">Description</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="" value={maxPresentations.state} onChange={maxPresentations.handleChange} required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                        <label htmlFor="max_presentations">Maximum Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="" value={maxAttendees.state} onChange={maxAttendees.handleChange} required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                        <label htmlFor="max_attendees">Maximum Attendees</label>
                    </div>

                    <div className="mb-3">
                      <select value={place.state} onChange={place.handleChange} required id="location" name="location" className="form-select">
                        <option key="defaultLocation" value="">Choose a Location</option>
                        {locations.map(place => <option key={place.href} value={place.id}>{place.name}</option>)}
                      </select>
                    </div>
                    <button className="btn btn-primary" type="submit">Create</button>
                  </form>
                </div>
              </div>
            </div>
    )
}