import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import './index.css';
import App from './App';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import NewConference from './new-conference';
import AttendConference from './attend-conference';
import NewPresentation from './new-presentation';
import MainPage from './MainPage';

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  if (response.ok) {
    const data = await response.json()
    const root = ReactDOM.createRoot(document.getElementById('root'));
    root.render(
      <React.StrictMode>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<App />}>
              <Route index element={<MainPage />} />
              <Route path="attendees" element={<AttendeesList attendees={data.attendees} />}/>
              <Route path="conferences/new" element={<NewConference />}/>
              <Route path="attendees/new" element={<AttendConference />}/>
              <Route path="locations/new" element={<LocationForm />}/>
              <Route path="presentation/new" element={<NewPresentation />}/>
            </Route>
          </Routes>

        </BrowserRouter>
        </React.StrictMode>
    );

  } else {
    console.error(response)
  }
}
reportWebVitals();
loadAttendees();
